defmodule Chan do
  def start_link do
    {:ok, spawn_link(&st_push/0)}
  end
  def start do
    {:ok, spawn(&st_push/0)}
  end

  def push(to, d, timeout\\:infinity) do
    send to, {:push, self, d}
    receive do
      {:rpush, ^to, ^d} -> :ok
    after
      timeout -> {:error, :timeout}
    end
  end
  def apush(to, d) do
    send to, {:push, d}
    :ok
  end
  def pull(from, timeout\\:infinity) do
    send from, {:pull, self}
    receive do
      {:rpull, ^from, d} ->
        {:ok, d}
    after
      timeout -> {:error, :timeout}
    end
  end

  def main(_args) do
    Process.flag :trap_exit, true
    recv spawn_link fn ->
      c = start_link
      run(c)
    end
  end

  defp run(c) do
    spawn_link(pdrain(c))
    IO.stream(:stdio, :line) |> Enum.each(&apush(c, &1))
  end

  defp recv(c) do
    receive do
      {:EXIT, from, :normal} ->
        :ok
      {:EXIT, ^c, :ratfor} ->
        IO.puts "ratfor"
      {:EXIT, _from, err} ->
        exit(err)
    end
  end
  defp pdrain(c) do
    fn ->
      do_pdrain(c)
    end
  end
  defp do_pdrain(c) do
    case pull(c) do
      {:ok, "error\n"} ->
        exit :watfor
      {:ok, "panic\n"} ->
        exit :ratfor
      {:ok, d} ->
        d = String.rstrip(d, ?\n)
        IO.puts d
        do_pdrain(c)
    end
  end

  defp st_pull({:push, from, d}) do
    receive do
      {:pull, to} ->
        send to, {:rpull, self, d}
        send from, {:rpush, self, d}
    end
    st_push
  end
  defp st_pull({:push, d}) do
    receive do
      {:pull, to} ->
        send to, {:rpull, self, d}
    end
    st_push
  end
  defp st_push do
    receive do
      msg = {:push, _from, _d} ->
        st_pull(msg)
      msg = {:push, _d} ->
        st_pull(msg)
    end
  end
end
